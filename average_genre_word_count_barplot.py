#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt

def bar_plot(lang_genre,title,ylabel):
	#Example format for lang_genre:
		#lang_genre = {'en':9,'it':4,'sp':2}
		#lang_genre = {'pop':9,'rock':4,'latin':2}

	#Reorder languages or genres in alphabetical order:
	x_values=list(sorted(lang_genre.keys()))
	y_values=list([lang_genre[k] for k in sorted(lang_genre)])
	y=[]
	y_err=[]
	for tuple in y_values:
		y.append(tuple[0])
		y_err.append(tuple[1])
	#Initialize
	N=len(lang_genre)
	width = 0.6  # the width of the bars
	fig, ax = plt.subplots(figsize=(15,9))
	#Plot bars
	#insert log=1 for logarithmic scale
	rects = ax.bar(np.arange(N)+(width/2), y, width, color='r', yerr=y_err) 
	
	#Add some text for labels, title and axes ticks
	ax.set_xlabel('Genre')
	ax.set_ylabel(ylabel.replace('_',' '))
	ax.set_title(title.replace('_',' '))
	ax.set_xticks(np.arange(N) + width)
	ax.set_xticklabels(x_values)

	#Add values of count to top of bars
	def autolabel(rects):
		# attach some text labels
		for rect in rects:
			height = rect.get_height()
			ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
					'%d' % int(height),
					ha='center', va='bottom')
	#autolabel(rects)
	
	#Show
	#plt.show()
	locs, labels = plt.xticks()
	plt.setp(labels, rotation=30)
	
	im_name = "bar_figs/Average features across genres/" + str(title)+".pdf"
	plt.savefig(im_name,format='pdf')
	plt.close(fig)