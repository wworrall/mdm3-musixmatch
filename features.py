import os
import sqlite3
import numpy as np

import utils

def feature(feat_name, genre=None, is_test=0):

    # make connection to new database and create cursor
    db = sqlite3.connect(os.path.join(utils.data_path, utils.db_name))
    c0 = db.cursor()
    
    if genre is None: # return all tracks
        feature_vals = c0.execute("SELECT " + feat_name + \
                " FROM genre_features WHERE is_test=(?) ORDER BY track_id",
                (is_test,)).fetchall()
    else: # return tracks of specific genre
        feature_vals = c0.execute("SELECT " + feat_name + \
                " FROM genre_features WHERE genre=(?) AND is_test=(?) ORDER BY track_id",
                (genre, is_test,)).fetchall()

    db.close()
    # extract values out of tuples and return list
    feature_vals = [ fv[0] for fv in feature_vals]

    return feature_vals


def word_count(genre=None, is_test=0):
    return feature("word_count", genre, is_test)

def unique_word_count(genre=None, is_test=0):
    return feature("unique_word_count", genre, is_test)

def track_pos(genre=None, is_test=0):
    return feature("track_pos", genre, is_test)

def track_neg(genre=None, is_test=0):
    return feature("track_neg", genre, is_test)

def track_obj(genre=None, is_test=0):
    return feature("track_obj", genre, is_test)
	
def swear_word_count(genre=None, is_test=0):
    return feature("swear_word_count", genre, is_test)

def language(genre=None, is_test=0):
    return feature("language", genre, is_test)


# extract entire genre_feature table (excluding track_ids)
def feature_table(is_test=0, genre_sample_size=None):

    # make connection to new database and create cursor
    db = sqlite3.connect(os.path.join(utils.data_path, utils.db_name))
    c0 = db.cursor()

    select_script = '''\
SELECT ''' + ", ".join(utils.feature_names) + '''\
 FROM genre_features WHERE is_test={0} ORDER BY track_id'''.format(is_test)

    c0.execute(select_script)
    table = c0.fetchall()
    table = [ list(row) for row in table ]

    db.close()

    # get corresponding genre list
    genre_list = utils.get_genre_list(is_test)

    if genre_sample_size is not None:
        # build a dict of indices for each genre in genre list
        ind_genres = {}
        for i, g in enumerate(genre_list):
            if g not in ind_genres:
                ind_genres[g] = np.array(i) # start the array of indices
            else:
                ind_genres[g] = np.append(ind_genres[g], i)

        # now take a random sample from each of the indices arrays
        for g, ind_arr in ind_genres.items():
            ind_genres[g] = np.random.choice(ind_arr,
                    size=genre_sample_size, replace=False)

        # now extract rows from table and genre list as appropriate
        all_ind = np.empty((0,0), dtype=np.int64) # append all indices arrays
        for g, ind_arr in ind_genres.items():
            all_ind = np.append(all_ind, ind_arr)

        genre_list = np.array(genre_list)[all_ind].tolist()
        table = [ table[row_ind] for row_ind in all_ind.tolist() ]

    return table, genre_list
