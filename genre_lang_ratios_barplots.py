import os

import features
import utils
import barplot as bp

def main():
        
    # get feature table
    feature_table, genre_list = features.feature_table()
    # extract language column
    langidx = utils.feature_names.index("language")
    lang_list = [ row[langidx] for row in feature_table ]

    # make a dict of dicts
    genre_lang_count = {}

    # add a dict for each genre:
    for genre in utils.genres:
        genre_lang_count[genre] = {}

    for genre, lang in zip(genre_list, lang_list):
        if lang is not None:
            if lang not in genre_lang_count[genre]:
                genre_lang_count[genre][lang] = 1
            else:
                genre_lang_count[genre][lang] += 1
        else:
            pass
    
    # find proportions of each language in each genre
    genre_lang_props = {}
    # add a dict for each genre:
    for genre in utils.genres:
        genre_lang_props[genre] = {}

    for genre_name in genre_lang_count.keys():
        genre_song_count = sum([ lang_count for lang_count in
                genre_lang_count[genre_name].values()])

        for lang_name in genre_lang_count[genre_name].keys():
            genre_lang_props[genre_name][lang_name] = \
                    genre_lang_count[genre_name][lang_name]/genre_song_count

    # find proportion of each language in the dataset
    lang_ratio = {}
    for lang in lang_list:
        if lang is not None:
            if lang not in lang_ratio:
                lang_ratio[lang] = 1
            else:
                lang_ratio[lang] += 1
    for lang_name in lang_ratio.keys():
        lang_ratio[lang_name] = lang_ratio[lang_name]/len(
                [ l for l in lang_list if l is not None])

    # find ratio of the proportion of languages in each genre to the
    # proportion of languages across the entire dataset
    genre_lang_ratios = {}
    # add a dict for each genre:
    for genre in utils.genres:
        genre_lang_ratios[genre] = {}

    for gn in genre_lang_ratios.keys():
        for lang, prop in genre_lang_props[gn].items():
            genre_lang_ratios[gn][lang] = prop / lang_ratio[lang]

    # make bar plots
    for genre, lang_ratios in genre_lang_ratios.items():
        title = 'Genre: ' + genre
        filename = os.path.join('bar_figs', genre + '.pdf')
        bp.bar_plot(lang_ratios, title, 'Language', 'Ratio',
                ylim=[0,10], log=True, hline=1, filename=filename)
	

if __name__ == "__main__":
	main()
