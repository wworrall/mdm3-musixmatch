import utils
from sklearn.neighbors import KNeighborsClassifier
from classifier import run_classifier
import numpy as np

def main():

    Xtrain, Xtest, ytrain, ytest = utils.get_data(train_size=200, test_size=25)
    
    Xtrain = utils.norm_feature_matrix(Xtrain)
    Xtest = utils.norm_feature_matrix(Xtest)
    
    for k in range(1,11):
        clf = KNeighborsClassifier(n_neighbors=k)
        name = "KNN_" + str(k)
        run_classifier(clf, "name", Xtest, Xtrain, ytest, ytrain)



if __name__ == "__main__":
	main()
