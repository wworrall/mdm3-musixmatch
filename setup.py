#!/usr/bin/env python3

"""file to setup and download required datasets and set anything else up - WIP"""

import csv
import os
import shutil
import sqlite3
import urllib.request
import zipfile
from nltk.corpus import stopwords
from nltk.corpus import wordnet
from nltk.corpus import sentiwordnet
from langdetect import detect_langs
from profanity import profanity

import utils

# dict of files to download in format local_file_name:url
downloads = {
        'mxm_dataset.db':'http://labrosa.ee.columbia.edu/millionsong/sites/default/files/AdditionalFiles/mxm_dataset.db',
        'mxm_dataset_readme.txt':'https://raw.githubusercontent.com/tbertinmahieux/MSongsDB/master/Tasks_Demos/Lyrics/README.txt',
        'msd_tagtraum_cd2.cls.zip':'http://www.tagtraum.com/genres/msd_tagtraum_cd2.cls.zip',
        'mxm_reverse_mapping.txt':'http://labrosa.ee.columbia.edu/millionsong/sites/default/files/mxm_reverse_mapping.txt',
        }

# make 'data' directory
try:
    os.makedirs(utils.data_path)
except OSError:
    if not os.path.isdir(utils.data_path):
        raise

# download all files:
for fname, url in downloads.items():
    print("Downloading file: {0}.".format(fname))
    urllib.request.urlretrieve(url, os.path.join(utils.data_path, fname))

# unzip all zip files and delete the zip files:
zipfilenames = [f for f in os.listdir(utils.data_path)
        if os.path.isfile(os.path.join(utils.data_path, f)) and f.endswith('.zip')]
for zfn in zipfilenames:
    # extract files from archive
    print("Unzipping and removing file: {0}.".format(zfn))
    zf = zipfile.ZipFile(os.path.join(utils.data_path, zfn))
    for zfinfo in zf.infolist():
        zf.extract(zfinfo, os.path.join(utils.data_path))
    # close and delete archive
    zf.close()
    os.remove(os.path.join(utils.data_path, zfn))



#
# create a new database and three cursors for nested operations
#
print("Copying database file {0} to {1}."
        .format('mxm_dataset.db', utils.db_name))
shutil.copyfile(os.path.join(utils.data_path, 'mxm_dataset.db'),
        os.path.join(utils.data_path, utils.db_name))

db = sqlite3.connect(os.path.join(utils.data_path, utils.db_name))
c0 = db.cursor()
c1 = db.cursor()
c2 = db.cursor()



#
# add genre_features table
#
print("Creating {0} using downloaded data:".format(utils.db_name))

# first make a dict with majority genres
genres = {}
with open(os.path.join(utils.data_path, 'msd_tagtraum_cd2.cls')) as f:
    datareader = csv.reader(f, delimiter='\t', quotechar='|')
    for row in datareader:
        # skip through comments
        if len(row) > 0 and row[0].startswith('#'):
            continue
        # if line is a track add to dict
        elif len(row) > 0 and row[0].startswith('TR'):
            trackid = row[0]
            genres[trackid] = row[1]
        # everything else not recognised - error
        else:
            raise ValueError()

# create 'genre_features' table collecting tracks which we don't have genres for
print("Creating genre_features table.")

c0.execute('''CREATE TABLE genre_features\
        (track_id, mxm_tid, genre, is_test)''')

missing_genres = []
prev_tid = ""
for row in c0.execute('''SELECT * FROM lyrics ORDER BY track_id'''):
    track_id = row[0]
    mxm_id = row[1]
    is_test = row[4]
    if track_id != prev_tid:
        try:
            c1.execute('''INSERT INTO genre_features VALUES (?,?,?,?)''',
                (track_id, mxm_id, genres[track_id], is_test))
        except KeyError:
            missing_genres.append(track_id)
    prev_tid = track_id

# delete tracks in 'lyrics' table which we dont have genres for
for mg_id in missing_genres:
    c0.execute('''DELETE FROM lyrics WHERE track_id=?''', (mg_id,))



#
# Add unstemmed_word column to words table
#
print("Adding unstemmed words to words table.")
c0.execute('''ALTER TABLE words ADD COLUMN unstemmed_word''')

word_rev_map = {}
with open(os.path.join(utils.data_path, 'mxm_reverse_mapping.txt')) as f:
    datareader = csv.reader((line.replace('<SEP>', ',') for line in f),
        delimiter=',', quotechar='|')
    for row in datareader:
        word_rev_map[row[0]] = row[1]

for row in c0.execute('''SELECT word FROM words ORDER BY ROWID'''):
    c1.execute('''UPDATE words SET unstemmed_word=(?) \
            WHERE word=(?)''',(word_rev_map[row[0]], row[0],))



#
# Add is_english_stopword column to words table
#
print("Adding is_en_stopword column to words table.")
c0.execute('''ALTER TABLE words ADD COLUMN is_en_stopword''')

en_stopwords = stopwords.words('english')
for row in c0.execute('''SELECT word FROM words ORDER BY ROWID'''):
    word = row[0]
    if word in en_stopwords:
        is_en_stopword = 1
    else:
        is_en_stopword = 0
    c1.execute('''UPDATE words SET is_en_stopword=(?) \
            WHERE word=(?)''',(is_en_stopword, word,))

			
			
#
# Add is_english_swearword column to words table
#			
print("Adding is_en_swearword column to words table.")
c0.execute('''ALTER TABLE words ADD COLUMN is_en_swearword''')

for row in c0.execute('''SELECT unstemmed_word FROM words ORDER BY ROWID'''):
    word = row[0]
    if(profanity.contains_profanity(word)):
        is_en_swearword = 1
    else:
        is_en_swearword = 0
    c1.execute('''UPDATE words SET is_en_swearword=(?) \
            WHERE word=(?)''',(is_en_swearword, word,))



#
# Add pos neg obj sentiment columns to words table
#
print("Adding word sentiment columns to words table.")
for name in ['sent_pos', 'sent_neg', 'sent_obj']:
    c0.execute('''ALTER TABLE words ADD COLUMN ''' + name)

#get pos neg obj sentiment value for word w
def get_sentiment(w):
    all_pos=[]
    all_neg=[]
    all_obj=[]
    ss = list(sentiwordnet.senti_synsets(w))
    if len(ss) == 0:
        return 3*(float('nan'),)
    else:
        for x in ss:
            all_pos.append(x.pos_score())
            all_neg.append(x.neg_score())
            all_obj.append(x.obj_score())
        return (sum(all_pos)/len(all_pos),
                sum(all_neg)/len(all_neg),
                sum(all_obj)/len(all_obj),)

# add to table
for row in c0.execute('''SELECT unstemmed_word FROM words ORDER BY ROWID'''):
    sentiment = get_sentiment(row[0])
    c1.execute('''UPDATE words SET sent_pos=(?), sent_neg=(?), sent_obj=(?) \
            WHERE unstemmed_word=(?)''', sentiment + (row[0],))



#
# Calculate feature values and add them to genre_features table
#
print("Adding features to genre_features table:")
for name in utils.feature_names:
    c0.execute('''ALTER TABLE genre_features ADD COLUMN ''' + name)


# word_count feature
print("    + word_count feature")
for row in c0.execute('''SELECT track_id \
        FROM genre_features ORDER BY track_id'''):
    track_id = row[0]
    c1.execute('''SELECT SUM(count) \
            FROM lyrics WHERE track_id=(?)''', (track_id,))
    word_count = c1.fetchone()[0]

    c1.execute('''UPDATE genre_features \
            SET word_count=(?) WHERE track_id=(?)''',(word_count, track_id,))


# unique_word_count feature
print("    + unique_word_count feature")
for row in c0.execute('''SELECT track_id \
        FROM genre_features ORDER BY track_id'''):
    track_id = row[0]
    c1.execute('''SELECT COUNT(*) \
            FROM lyrics WHERE track_id=?''',(track_id,))
    unique_word_count = c1.fetchone()[0]

    c1.execute('''UPDATE genre_features SET unique_word_count=(?) \
            WHERE track_id=(?)''',(unique_word_count, track_id,))


# track sentiment features
print("    + sentiment features: track_pos, track_neg, track_obj")
for row0 in c0.execute('''SELECT track_id \
        FROM genre_features ORDER BY track_id'''):
    track_id = row0[0]
    pos_counter = 0
    neg_counter = 0
    obj_counter = 0

    word_count = c1.execute('''SELECT word_count FROM genre_features \
            WHERE track_id=(?)''', (track_id,)).fetchone()[0]

    for row1 in c1.execute('''SELECT word, count FROM lyrics \
            WHERE track_id=(?)''', (track_id,)):
        word = row1[0]
        count = row1[1]
        sent_pos, sent_neg, sent_obj = c2.execute('''SELECT sent_pos, \
                sent_neg, sent_obj FROM words WHERE word=(?)''',
                (word,)).fetchone()
        try:
            pos_counter += sent_pos*count
            neg_counter += sent_neg*count
            obj_counter += sent_obj*count
        except TypeError:
            # we dont have sentiment values for this word so exclude it from
            # word_count
            word_count -= count

    # when we have no words with sentiment scores, word_count will be zero
    try: 
        track_pos = pos_counter/word_count
        track_neg = neg_counter/word_count
        track_obj = obj_counter/word_count
    except ZeroDivisionError:
        track_pos, track_neg, track_obj = 3*(float('nan'),)

    c1.execute('''UPDATE genre_features SET track_pos=(?), track_neg=(?), \
            track_obj=(?) WHERE track_id=(?)''',
            (track_pos, track_neg, track_obj, track_id,))


# language feature
print("    + language feature")
repetitions = 5 #number of repetitions for averaging
threshold = 0.85
for row0 in c0.execute('''SELECT track_id \
        FROM genre_features ORDER BY track_id'''):
    track_id = row0[0]
    all_words = ""
    likeness = 0

    # build up string of all words in song
    for row1 in c1.execute('''SELECT word FROM lyrics \
            WHERE track_id=(?)''', (track_id,)):
        word = row1[0]
        # convert from word to unstemmed words
        unstemmed_word = c2.execute('''SELECT unstemmed_word FROM words \
                WHERE word=(?)''', (word,)).fetchone()[0]
        all_words = " ".join([all_words, unstemmed_word])

    # repeat and take average because detect_langs is not deterministic
    for i in range(repetitions):
        langs = detect_langs(all_words)
        if len(langs) == 1:
            language, likeness_str = langs[0].__repr__().split(':')
            likeness += float(likeness_str)

    # now take average 'likeness'
    likeness = likeness/repetitions

    # if likeness less than our confidence threshold don't set language
    if likeness < threshold:
        language = None

    c1.execute('''UPDATE genre_features SET language=(?) \
                WHERE track_id=(?)''', (language, track_id,))


# swear_word_count feature
print("    + swear_word_count feature")
for row0 in c0.execute('''SELECT track_id \
        FROM genre_features ORDER BY track_id'''):

    track_id = row0[0]
    swear_word_count=0

    for row1 in	c1.execute('''SELECT word, count \
            FROM lyrics WHERE track_id=?''',(track_id,)):

        word, count = row1
        c2.execute('''SELECT is_en_swearword FROM words \
                WHERE word=(?)''',(word,))
        is_en_swearword = c2.fetchone()[0]

        if is_en_swearword:
            swear_word_count += count

    c1.execute('''UPDATE genre_features SET swear_word_count=(?) \
                    WHERE track_id=(?)''',(swear_word_count, track_id,))


#
# commit changes and close connection
#
db.commit()
db.close()
