import utils
import numpy as np
from sklearn.svm import LinearSVC
from sklearn.svm import SVC
from classifier import run_classifier
from utils import sparse_word_count_matrix

def main():

	Xtest, ytest = sparse_word_count_matrix(is_test=1, genre_sample_size=25)
	Xtrain, ytrain = sparse_word_count_matrix(is_test=0, genre_sample_size=200)
	
	clf = LinearSVC()
	run_classifier(clf, "LinearSVC", Xtest, Xtrain, ytest, ytrain)
	
	clf = SVC(kernel="linear")
	run_classifier(clf, "LinearSVC_using_SVC", Xtest, Xtrain, ytest, ytrain)


if __name__ == "__main__":
	main()
