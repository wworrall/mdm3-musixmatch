#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt

def bar_plot(data_dict, title, xlabel, ylabel,
        ylim=None, log=False, hline=None, filename=None):
    #Example format for data_dict:
    # data_dict = {'en':9,'it':4,'sp':2}

    #Reorder data_dict in alphabetical order:
    x_values=list(sorted(data_dict.keys()))
    y_values=list([data_dict[k] for k in sorted(data_dict)])

    #Initialize
    N=len(data_dict)
    width = 0.6  # the width of the bars
    fig, ax = plt.subplots(figsize=(15,9))

    #Plot bars
    #insert log=True for logarithmic scale
    rects = ax.bar(np.arange(N)+(width/2), y_values, width, color='b',
            log=log) 
    # optional horizontal line
    if hline is not None:
        ax.axhline(y=hline)
    if ylim is not None:
        ax.set_ylim(ylim)


    #Add some text for labels, title and axes ticks
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    ax.set_xticks(np.arange(N) + width)
    ax.set_xticklabels(x_values)
    ax.tick_params(axis='both', which='major', labelsize=14)
    ax.tick_params(axis='both', which='minor', labelsize=14)

    #Add values of count to top of bars
    def autolabel(rects):
        # attach some text labels
        for rect in rects:
            height = rect.get_height()
            ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                    '%d' % int(height),
                    ha='center', va='bottom')
    #autolabel(rects)

    if filename is None:
        plt.show()
    else:
        plt.savefig(filename,format='pdf')
        plt.close(fig)
