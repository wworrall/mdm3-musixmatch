import utils
from sklearn.decomposition import PCA
from classifier import run_classifier
import numpy as np
from matplotlib import colors
import matplotlib.pyplot as plt

def main():
    data = utils.get_data(train_len=1000, test_len=100)

    Xtrain = data["Xtrain"]
    Xtest = data["Xtest"]
    ytrain = data["ytrain"] 
    ytest = data["ytest"]

    run_pca(Xtrain,ytrain)


def run_pca(Xtrain,ytrain):
    pca = PCA(n_components=2)
    transformed = pca.fit_transform(Xtrain)

    fig = plt.figure()
    ax = fig.add_axes((0.1,0.1,0.8,0.8,)) # l,b,w,h
    color_map = plt.get_cmap("hsv")
    genre_list = utils.get_genre_list()
    colors = color_map(np.linspace(0,1,15))

    for i, genre in enumerate(utils.genres):
        # find indices of genre in genre_list
        indices = []
        for idx, g in enumerate(genre_list):
            if idx >= 1000:
                break
            if g == genre:
                indices.append(idx)
        data = transformed[indices, :]
        ax.scatter(data[:,0], data[:,1],
                label=genre, color=colors[i])

        plt.legend(loc="upper left", prop={'size':9})
    plt.show()


if __name__ == "__main__":
	main()
