import numpy as np
import features
from sklearn.naive_bayes import MultinomialNB
from classifier import run_classifier
from utils import sparse_word_count_matrix
from utils import get_genre_list


def main():
	#get data
	Xtest, ytest = sparse_word_count_matrix(is_test=1, genre_sample_size=25)
	Xtrain, ytrain = sparse_word_count_matrix(is_test=0, genre_sample_size=None)
	print("Got Features")
	print("Got Labels")
	
	clf = MultinomialNB(alpha=1)
	run_classifier(clf, "Naive Bayes", Xtest, Xtrain, ytest, ytrain)


if __name__ == "__main__":
	main()
