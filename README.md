Mathematical and Data Modelling project carried out by third year students 
of the Department of Engineering Mathematics at the University of Bristol.

Report found here: https://www.overleaf.com/read/tdfsbkmkbdns

The aim of this project is to infer song genre from lyric data given by the 
Musixmatch official lyrics collection of the Million Song Dataset. Genre labels
are used for supervised learning techniques and are provided by Tagraum 
Industries. A custom database is generated representing the intersection of 
tracks covered by the Musixmatch dataset and labelled by Tagtraum. To construct
this database which includes features used for classification, pull from this
repo and run setup.py (python3 required).

In order to run, this repo requires lang-detect to be installed locally,
which can be found at:
https://pypi.python.org/pypi/langdetect

To install the database, simply run setup.py
Note: Installing the database can take a large amount of time (in the magnitude
of hours is not unexpected).
