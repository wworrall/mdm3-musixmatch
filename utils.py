#!/usr/bin/env python3

import csv
import os
import sqlite3
from scipy import sparse
import numpy as np
from sklearn.preprocessing import Imputer

import features

data_path = 'data'
db_name = 'lyrics_genres_features.db'

genres = ['Blues',
        'Country',
        'Electronic',
        'Folk',
        'Jazz',
        'Latin',
        'Metal',
        'New Age',
        'Pop',
        'Punk',
        'Rap',
        'Reggae',
        'RnB',
        'Rock',
        'World',
	]

feature_names = [
        'word_count',
        'unique_word_count',
        'track_pos',
        'track_neg',
        'track_obj',
        'language',
	'swear_word_count',
        ]
		
language_names = [
        'af',
        'ca',
        'cs',
        'cy',
        'da',
        'de',
        'en',
        'es',
        'et',
        'fi',
        'fr',
        'hr',
        'hu',
        'id',
        'it',
        'nl',
        'no',
        'pl',
        'pt',
        'ro',
        'sk',
        'sl',
        'so',
        'sv',
        'sw',
        'tl',
        'tr',
        ]

def import_words():
    db = sqlite3.connect(os.path.join(data_path, db_name))
    c0 = db.cursor()

    c0.execute('''SELECT word FROM words ORDER BY ROWID''')
    all_words = c0.fetchall()

    db.close()
    return [ w[0] for w in all_words ]


def import_unstemmed_words():
    db = sqlite3.connect(os.path.join(data_path, db_name))
    c0 = db.cursor()

    c0.execute('''SELECT unstemmed_word FROM words ORDER BY ROWID''')
    all_us_words = c0.fetchall()

    db.close()
    return [ w[0] for w in all_us_words ]


def sparse_word_count_matrix(is_test=0, genre_sample_size=None):
    db = sqlite3.connect(os.path.join(data_path, db_name))
    c0 = db.cursor()
    c1 = db.cursor()

    no_rows = c0.execute('''SELECT COUNT(*) FROM \
            genre_features WHERE is_test=(?)''',(is_test,)).fetchone()[0]
    no_cols = len(import_words()) # 5000 words

    I = [] # row indices
    J = [] # column indices
    V = [] # values

    prev_tid = ""
    row_counter = -1 # is increased to 0 on first iteration
    for row in c0.execute('''SELECT track_id, word, count \
            FROM lyrics WHERE is_test=(?) ORDER BY track_id''',(is_test,)):
        track_id, word, count = row

        if track_id != prev_tid:
            row_counter += 1

        # find index of word, in the database these begin at 1 so we take one
        # away from result so we count from 0
        word_index = c1.execute('''SELECT ROWID FROM words \
                WHERE word=(?)''', (word,)).fetchone()[0] - 1

        I.append(row_counter)
        J.append(word_index)
        V.append(count)

        prev_tid = track_id

    db.close()

    sparse_mat = sparse.coo_matrix((V,(I,J)), shape=(no_rows, no_cols))

    # get corresponding genre list as numpy array
    genre_arr = np.array(get_genre_list(is_test))

    # now remove rows randomly until we have achieved genre_sample_size
    if genre_sample_size is not None:
        # build a dict of indices for each genre in genre list
        ind_genres = {}
        for i, g in np.ndenumerate(genre_arr):
            if g not in ind_genres:
                ind_genres[g] = np.array(i[0]) # start the array of indices
            else:
                ind_genres[g] = np.append(ind_genres[g], i[0])

        # now take a random sample from each of the indices arrays
        for g, ind_arr in ind_genres.items():
            ind_genres[g] = np.random.choice(ind_arr,
                    size=genre_sample_size, replace=False)

        # now extract rows from sparse matrix and genre list as appropriate
        all_ind = np.empty((0,0), dtype=np.int64) # append all indices arrays
        for g, ind_arr in ind_genres.items():
            all_ind = np.append(all_ind, ind_arr)

        sparse_mat = sparse_mat.tocsr()
        sparse_mat = sparse_mat[all_ind, :]
        genre_arr = genre_arr[all_ind]

    return sparse_mat, genre_arr


def get_genre_list(is_test=0):
    db = sqlite3.connect(os.path.join(data_path, db_name))
    c0 = db.cursor()

    gl = c0.execute('''SELECT genre FROM genre_features WHERE is_test=(?) \
            ORDER BY track_id''', (is_test,)).fetchall()

    db.close()

    # extract genres from tuples and return
    return [ g[0] for g in gl ]


def sum_genres(is_test=0):
    """returns all genres and a sum of each genre occurrence in genre labels
    that exist in the database. Accepts optional is_test param (default = 0)
    Output is a dict of genre:count. genre is a str, count is int."""

    genre_list = get_genre_list(is_test)
    genre_counts = {}
    for genre in genre_list:
            if genre not in genre_counts:
                    genre_counts[genre] = 1
            else:
                    genre_counts[genre] += 1

    return genre_counts


def format_conf_matrix(cm, for_latex=False):
    if for_latex:
        joiner = " & "
        end = " \\\\\n"
    else:
        joiner = " "
        end = "\n"

    output = ""
    for row in cm:
        output += joiner.join([ "{0:>4}".format(el) for el in row ])
        output += end

    return output


def norm_feature_matrix(fm, genre_included=False):
    '''Normalise word_count and unique_word counts using maximum values taken
    from training data'''

    # make connection to new database and create cursor
    db = sqlite3.connect(os.path.join(data_path, db_name))
    c0 = db.cursor()

    max_wc = c0.execute('''SELECT MAX(word_count) \
            FROM genre_features WHERE is_test=0''').fetchone()[0]
    max_uwc = c0.execute('''SELECT MAX(unique_word_count) \
            FROM genre_features WHERE is_test=0''').fetchone()[0]
    db.close()

    shift = 0
    if genre_included:
        shift += 1

    # find column indexes and add one to account for genres first column
    wc_idx = feature_names.index("word_count") + shift
    uwc_idx = feature_names.index("unique_word_count") + shift

    fm = np.asfarray(fm)
    fm[:, wc_idx] = np.divide(fm[:,wc_idx], float(max_wc))
    fm[:, uwc_idx] = np.divide(fm[:,uwc_idx], float(max_uwc))

    return fm


def lang_mappings():
    mapping = {}
    for lang in language_names:
        if not lang in mapping:
            mapping[lang] = len(mapping)
    return mapping


def encode_languages(features):
    '''converts data with language as string to data with language as binary
    matrix find column index of "language" by finding its position in
    utils.feature_names and then add 1 to account for genre first column'''
    lang_idx = feature_names.index("language") 

    mappings = lang_mappings()
    output = np.zeros((1, len(mappings)+features.shape[1]-1))
    for track in features:
        blank = np.zeros(len(mappings))
        if track[lang_idx] in mappings:
            blank[mappings[track[lang_idx]]] = 1
        track = np.delete(track, lang_idx)
        track = np.append(track,blank)
        output = np.append(output,[track],axis=0)
    output = np.delete(output, (0), axis=0)

    return output


def get_data(train_size=None, test_size=None):
    '''loads data from database and returns it as dict split into test, train
    and x,y encodes language as matrix'''
 
    Xtrain, ytrain = (features.feature_table(is_test=0, genre_sample_size=train_size))
    Xtest, ytest = (features.feature_table(is_test=1, genre_sample_size=test_size))
        
    Xtrain = np.array(Xtrain)
    Xtest= np.array(Xtest)
    ytrain = np.array(ytrain)
    ytest = np.array(ytest)
    
    Xtrain = encode_languages(Xtrain)
    Xtest = encode_languages(Xtest)

    #Fix missing data
    imp = Imputer(missing_values='NaN', strategy='mean', axis=0)
    imp.fit(Xtrain)
    Xtrain = imp.transform(Xtrain)
    Xtest = imp.transform(Xtest)

    return Xtrain, Xtest, ytrain, ytest


if __name__ == "__main__":
    tracks = import_tracks()
    nogenre = 0
    for trackid, trackdict in tracks.items():
        if trackdict['genre'] is None:
            nogenre +=1

    print('No. tracks without a genre = %d' % nogenre)
    print('No. tracks with a genre = %d' % (len(tracks)-nogenre) )

