#average genre word count bar plot
import features
import utils
import average_genre_word_count_barplot as bp
import numpy
import statistics

feat_list = utils.feature_names
feat_list.remove('language')

# each feature except language
for feat in feat_list:
	bar_data={}
	for gen in utils.genres:	
		values = features.feature(feat,gen)
		values = [x for x in values if x is not None]
		bar_data[gen] = (sum(values)/len(values),statistics.stdev(values))
	bp.bar_plot(bar_data,'Average '+feat+' per genre','Average '+ feat)

# word/unique word count ratio
bar_data={}
feat='word_count over unique_word_count'
for gen in utils.genres:	
		values1 = features.feature('word_count',gen)
		values2 = features.feature('unique_word_count',gen)
		bar_data[gen] = (sum(values1)/len(values1))/(sum(values2)/len(values2))
#bp.bar_plot(bar_data,'Average '+feat+' per genre','Average ' + feat)


