data = open('data/mxm_dataset_train.txt', 'r')
tags = open('data/msd_tagtraum_cd2c.txt', 'r')
out = open('data/track_langs.txt', 'w')
from langdetect import detect_langs
from langdetect import detect
import linecache

def get_line(data):
	line_count = 0
	for line in data:
		line_count += 1
		if line_count == 18:
			output = line
	return output

def get_indices(line):	
	output = {}
	ignore_lang = [456, 531, 666, 718, 787, 1046, 1299, 1895, 2197,2356, 2474, 2714, 2746, 3035, 3506, 3526, 3776, 4277, 4286, 4306, 4337, 4746, 4752, 4886, 4954, 4967]
	ignore_char = [1262, 1424, 1597, 1987, 2050, 2136, 2388, 2490, 2793, 2814, 2972, 3089, 3505, 3768, 3876, 3905, 4170, 4208,4397, 4421, 4565, 4579, 4609, 4819, 4938]
	words = line.split(",")
	words[0] = words[0][1:]
	for i in range(0, 5000):
		if not (i in ignore_char) or (i in ignore_lang):
			output[i+1] = words[i]
	last_word = output[5000].split("\n")
	output[5000] = last_word[0] 
	return output	

def word_mappings(word_dict, test_words_line):	
	print("loading mxm words line...")
	#test_words_line = linecache.getline('data/mxm_dataset_test.txt', 18)[1:]
	test_words_list = test_words_line.split(",")
	#load mxm mapping
	#create dictionary from mapping
	print("creating dictionary from mxm mapping...")
	dictList = {}
	with open('data/mxm_reverse_mapping.txt', 'r') as f:
		for line in f:
			(first,second)=line.rstrip().split("<SEP>")
			dictList[first]=second		
	#update words list using dictionary
	print("updating words list using dictionary...")

	updated_dict = {}
	for w in word_dict:
		updated_dict[w] = dictList[word_dict[w]]
	return updated_dict
	#print(updated_dict)
	
def process_genre(line):
	# receives line from genre file and extracts trackID and genre from string
	line_separated = line.split("\t")
	genre = line_separated[1].split("\n")
	output = {line_separated[0]:genre[0]}	
	return output
	
def get_genre_dict(filename):
	# loops through lines in genre file and creates dictionary of form {trackID: genre}
	genre_dict = {}
	for line in filename:
		if line[0][0] == 'T':
			genre_dict.update(process_genre(line))
	return genre_dict
	
def get_word_dicts(trackfile, genredict):
	#loops through all tracks and returns dictionary containing array of features, lists of corresponding trackIDs and genres
	output_dict = {}
	for line in trackfile:
		if line[0][0] == 'T':# and len(output_dict) < 5:
			track_word_dict = get_words(line, genredict) #gets words for each track in dictionary form
			if bool(track_word_dict):	#dict will be null if that track has no genre label
				line_separated = line.split(",")
				output_dict[line_separated[0]] = track_word_dict
	return output_dict
	
def get_words(line, genres):
	# receives line as string and separates it into words, removing newline characters
	line_separated = line.split(",")
	words = {}
	if line_separated[0] in genres:
		last_word = line_separated[-1].split("\n")
		del line_separated[-1]
		line_separated.append(last_word[0])
		for x in range(2, len(line_separated)-1):
			word_separated = line_separated[x].split(":")
			words[word_separated[0]] = word_separated[1]
	return words
	
def get_track_langs(tracks, word_list):
	# receives dict of form {track:{word:count}} and returns dict of form {track:language}} 
	output = {}
	repetitions = 5 #number of repetitions for averaging
	threshold = 0.85
	successes = 0
	for track in tracks:
		concat_string = ""
		likeness = 0
		for word in tracks[track]:	#check if word or wordID
			if int(word) in word_list:
				concat_string = concat_string + " " + word_list[int(word)] 
		for i in range(repetitions):
			langs = detect_langs(concat_string)
			if len(langs) == 1:
				thing = langs[0].__repr__()
				language_likeness = thing.split(":")
				likeness += float(language_likeness[1])
		likeness = likeness/repetitions
		if likeness > threshold:
			output[track] = language_likeness[0]
			successes += 1
	return output
	
def write2file(dict2write, outputfile):
	for key in dict2write:
		outputstr = key + "," + dict2write[key] + "\n"
		outputfile.write(outputstr)
		

word_line = get_line(data)
data.seek(0)
word_indices = get_indices(word_line)
word_indices = word_mappings(word_indices, word_line)
print("Dictionary {index:word} extracted successfully")
genre_dict = get_genre_dict(tags)	
print("Successfully extracted dictionary of genre labels")
word_dicts = get_word_dicts(data, genre_dict)
print("Successfully extracted word dicts for " + str(len(word_dicts)) + " tracks.")
lang_dict = get_track_langs(word_dicts, word_indices)
print("Successfully extracted languages for " + str(len(lang_dict)) + " tracks.")
write2file(lang_dict, out)
print("File write complete")

out.close()
data.close()
tags.close()


		
