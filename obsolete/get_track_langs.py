from langdetect import detect_langs
from langdetect import detect


def get_track_langs(tracks):
	# receives dict of form {track:{word:count}} and returns dict of form {track:language}} 
	output = {}
	repetitions = 5 #number of repetitions for averaging
	threshold = 0.85
	successes = 0
	for track in tracks:
		concat_string = ""
		likeness = 0
		for word in tracks[track]:	#check if word or wordID
			concat_string = concat_string + " " + word 
		for i in range(repetitions):
			langs = detect_langs(concat_string)
			if len(langs) == 1:
				thing = langs[0].__repr__()
				language_likeness = thing.split(":")
				likeness += float(language_likeness[1])
		likeness = likeness/repetitions
		if likeness > threshold:
		
			output[track] = language_likeness[0]
			successes += 1
	print("Number of tracks for which language successfully found = " + str(successes))
	print("Number of tracks for which language NOT successfully found = " + str(len(tracks) - successes))
	return output

def get_word_lang_count(tracks, track_lang):	
	# receives dict of tracks and track languages from get_track_langs and returns dict of form {word:{language:count}} 
	output = {}
	languages = {}
	for track in track_lang:
		if not track_lang[track] in languages:
			languages[track_lang[track]] = 0
	for track in track_lang:
			for word in tracks[track]:
				if not word in output:
					output[word] = languages.copy()
				output[word][track_lang[track]] += 1
				
	return output
	
def find_uniqueness(word_counts):
	# receives wordcounts and returns dict of form {word:unique language}
	output = {}
	for word in word_counts:
		lang_occ = []
		lang_occ_ratio = []
		for lang in word_counts[word]:
			if not word_counts[word][lang] == 0:
				lang_occ.append(lang)
				lang_occ_ratio.append(word_counts[word][lang])
		if len(lang_occ) == 1:
			output[word] = lang_occ[0]
		else:
			# could do another cutoff ie. 90% eng, 10% span --> eng
			# would need a minimum word occurence for this though... might be implied by percentage?
			for lang in lang_occ:
				if lang_occ_ratio[lang] > 0.8:
					output[word] = lang_occ[0]
				
	return output


def main():
	data = {"track1":{"the":2, "get":6, "try":1, "farm":8, "song":2, "bored":7, "suitable":7}, "track2":{"Haus":2, "trauben":6, "du":1, "wilkommen":8, "schlafen":2, "langweilig":7, "lecker":7}, "track3":{"hi":1,"what":2, "que":4}} # Replace with call to database
	track_langs = get_track_langs(data)			
	#print(track_langs)
	word_counts = get_word_lang_count(data, track_langs)
	#print(word_counts)
	unique_words = find_uniqueness(word_counts)
	#print(unique_words)

if __name__ == "__main__":
	main()
