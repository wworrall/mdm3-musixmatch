import numpy as np
import features
from classifier import run_classifier
from sklearn.svm import LinearSVC
import sklearn.metrics as skl_metrics
from sklearn.linear_model import Perceptron
import utils

def main():
    data = utils.get_data(train_len=10000, test_len=1000)

    Xtrain = data["Xtrain"]
    Xtest = data["Xtest"]
    ytrain = data["ytrain"] 
    ytest = data["ytest"]
    
    for idx, genre in enumerate(ytrain):
        if not genre == "Rap":
            ytrain[idx] = "Not Rap"    
    for idx, genre in enumerate(ytest):
        if not genre == "Rap":
            ytest[idx] = "Not Rap"

    rap_labels = ["Rap", "Not Rap"]
    clf = LinearSVC()
    clf = clf.fit(Xtrain, ytrain)
    predictions = clf.predict(Xtest)
    
    conf_matrix = skl_metrics.confusion_matrix(ytest,
                predictions, labels=rap_labels)
    acc = skl_metrics.accuracy_score(ytest, predictions)
    wap = skl_metrics.precision_score(ytest, predictions,
                average='weighted',pos_label=None)
    print(utils.format_conf_matrix(conf_matrix))
    print("Performance of Rap SVC classifier:")
    print("Weighted Average Precision: " + str(wap))
    print("Accuracy: " + str(acc) + "\n")
    
if __name__ == "__main__":
    main()
