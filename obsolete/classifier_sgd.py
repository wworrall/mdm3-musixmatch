from sklearn.linear_model import SGDClassifier

import features

# Read in X as data values (2D, comparing features)
# Read in Y as genre values (0,1,2 etc. for genres
# Could do 1 to 1 comparison or multiple simultaneous (!)

# Output prediction

pop_word_count = features.feature("word_count","Pop")
rock_word_count = features.feature("word_count","Rock")
pop_unique_word_count = features.feature("unique_word_count","Pop")
rock_unique_word_count = features.feature("unique_word_Count","Rock")


X = []
y = []
# This is really bad... better implementation?
for i in range(0,len(pop_word_count)):
	X.append([pop_word_count[i], pop_unique_word_count[i]])
	y.append(0)
for i in range(0,len(rock_word_count)):
	X.append([rock_word_count[i],rock_unique_word_count[i]])
	y.append(1)
#print(len(X))
#print(len(y))
clf = SGDClassifier(loss="hinge", penalty="l2")
clf.fit(X, y)
SGDClassifier(alpha=0.0001, average=False, class_weight=None, epsilon=0.1,
       eta0=0.0, fit_intercept=True, l1_ratio=0.15,
       learning_rate='optimal', loss='hinge', n_iter=5, n_jobs=1,
       penalty='l2', power_t=0.5, random_state=None, shuffle=True,
       verbose=0, warm_start=False)
print(clf.coef_)
print(clf.intercept_)

# Below here is for testing purposes.
pop = 0
rock = 0
for i in range(0,1001):
	for j in range(0,1001):
		predict = clf.predict([[i,j]])
#		print("predict[" + str(i)  + ".," + str(j) + ".] " + str(predict))
		if predict == 0:
			pop += 1
		else:
			rock += 1
print("pop: " + str(pop) + "\nrock: " + str(rock))
# Always 1 category. Values too mixed?
# Almost definitely need more features.
