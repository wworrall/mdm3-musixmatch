#REQUIRES NLTK
from nltk.corpus import wordnet
import itertools
import linecache
import csv
import numpy

def main():
	#load words line
	print("loading mxm words line...")
	test_words_line = linecache.getline('G:/MDM/Code/Data/mxm_dataset_test.txt', 18)[1:]
	test_words_list = test_words_line.split(",")
	
	#load mxm mapping
	#create dictionary from mapping
	print("creating dictionary from mxm mapping...")
	dictList = {}
	with open('G:/MDM/Code/Data/mxm_reverse_mapping.txt', 'r') as f:
		for line in f:
			(first,second)=line.rstrip().split("<SEP>")
			dictList[first]=second		
	#update words list using dictionary
	print("updating words list using dictionary...")
	updated_list=[]
	for w in test_words_list:
		updated_list.append(dictList.get(w.rstrip(), w.rstrip()))		
	
	#load stopwords_list
	stopwords_list=[]
	with open('stopwords.csv','r') as stopwords:
		for line in stopwords.readlines()[3:]:
			stopwords_list.append(line.rstrip().split(',')[0])
	#update words list using stopwords_list
	updated_list_2 = [item for item in updated_list if item not in stopwords_list]
	
	#save updated words_list
	print("saving words_list...")
	with open('words_list_nomax.txt','w') as thefile:
		for item in updated_list_2:
			try:
				thefile.write("%s\n" % item)	
			except:
				thefile.write("%.4f\n" % -1)
	print("saved to \"words_list.txt\"")
	
	#for all combinations (no repeats), get wup values and append to wup_array
	print("getting wup values...")
	with open('pdist_wup_array_nomax.txt','w') as thefile:
		for w1, w2 in itertools.combinations(updated_list_2, 2):
			try:
				thefile.write("%.4f\n" % wup(w1,w2))			
			except:
				thefile.write("%.4f\n" % -1)
			print(updated_list.index(w1))
	print("saved to \"pdist_wup_array.txt\"")
	
	
#get wup value between word1 and word2
def wup(word1, word2):
	try:
		wordFromList1=wordnet.synsets(word1)[0]
		wordFromList2=wordnet.synsets(word2)[0]
	except:
		return -1 #returns string 'error' if word not found
	else:
		return wordFromList1.wup_similarity(wordFromList2) #if words found, returns wup value
	#synset_comb=numpy.empty(shape=(0, 0))
	#for x,y in itertools.product(wordnet.synsets(word1),wordnet.synsets(word2)):
	#	wupval=x.wup_similarity(y)
	#	if wupval == None:
	#		synset_comb=numpy.append(synset_comb,-1)
	#	else:
	#		synset_comb=numpy.append(synset_comb,wupval)
	#return numpy.amax(synset_comb)
	
	
if __name__ == "__main__":
	main()