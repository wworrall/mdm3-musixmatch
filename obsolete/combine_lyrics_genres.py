# OUTDATED, SEE SQL IMPLEMENTATION

import os.path
import shutil
import glob
import string

def main():
	# Check if parsed lyric file is made
	train = 'mxm_dataset_train'
	test = 'mxm_dataset_test'
	filecheck(train)
	filecheck(test)

	# Order files
	filesort(train)
	filesort(test)
	
	
	# Append genre data to lyric data
	
	
def build_mxm_dataset_parsed(filename):
	print('Parsing dataset') # debug
	# Create comment-free file
	oldmainfile = open('Data/' + filename + '.txt', 'r')
	newmainfile = open('Data/' + filename + '_parsed.txt','w')
	
#	keyfound = False
	for line in oldmainfile:
		if not line.startswith("#"):
			if not line.startswith("%"):
				newmainfile.write(line)
#			else:
#				if keyfound == False:
#					newmainfile.write(line)
#					keyfound = True
				
	oldmainfile.close()
	newmainfile.close()

def filecheck(filename):
	if not os.path.isfile('Data/' + filename + '_parsed.txt'):
		print('No parsed dataset for ' + filename + '.txt') # debug
		build_mxm_dataset_parsed(filename)
		
def filesort(filename):
	mainfile = open('Data/' + filename + '_parsed.txt','r')
	outfile = open('Data/' + filename + '_sorted.txt','w')
	
	for line in sorted(mainfile):
		outfile.write(line)
	

if __name__ == "__main__":
	main()