# OUTDATED, SEE SQL IMPLEMENTATION

import os.path
import shutil
import glob
import string
import linecache

def main():	
	#	GET WORDS LINE FROM TRAIN AND CHECK FOR DUPLICATES
	train_words_line = linecache.getline('Data/' + 'mxm_dataset_train' + '.txt', 18)[1:]
	train_words_list = train_words_line.split(",")
	list_name_train_words = 'train_words_list'
	
	check_for_duplicates(train_words_list,list_name_train_words)
#	write_to_file(train_words_list,list_name_train_words)
	
	#	GET WORDS LINE FROM TEST AND CHECK FOR DUPLICATES
	test_words_line = linecache.getline('Data/' + 'mxm_dataset_test' + '.txt', 18)[1:]
	test_words_list = test_words_line.split(",")
	list_name_test_words = 'test_words_list'
	
	check_for_duplicates(test_words_list,list_name_test_words)
#	write_to_file(test_words_list,list_name_test_words)
	
	
	
	#	GET IDS LIST FROM TRAIN
	oldmainfile_train = open('Data/' + 'mxm_dataset_train' + '.txt', 'r')
	train_ids_list=[]
	for line in oldmainfile_train:
		if not line.startswith("#"):
			if not line.startswith("%"):
				idtag=line.split(',', 1)[0]
				train_ids_list.append(idtag)
	oldmainfile_train.close()
	#	GET IDS LIST FROM TEST
	oldmainfile_test = open('Data/' + 'mxm_dataset_test' + '.txt', 'r')
	test_ids_list=[]
	for line in oldmainfile_test:
		if not line.startswith("#"):
			if not line.startswith("%"):
				idtag=line.split(',', 1)[0]
				test_ids_list.append(idtag)
	oldmainfile_test.close()
	
	#	COMBINE IDS LISTS FROM TRAIN AND TEST AND CHECK FOR DUPLICATES
	full_ids_list = train_ids_list + test_ids_list

	list_name_ids = 'full_ids_list'
	check_for_duplicates(full_ids_list,list_name_ids)
#	write_to_file(full_ids_list,list_name_ids)
	
def check_for_duplicates(list,list_name):
	if len(list)!=len(set(list)):print "duplicate found in " + list_name
	else:print "no duplicates found in " + list_name
	
def write_to_file(list,list_name):
	newfile = open('Data/' + list_name + '.txt','w')
	for item in list:
		newfile.write("%s\n" % item)
	newfile.close()
	
if __name__ == "__main__":
	main()