import math

def get_word_counts(lyric_dict):
	genre_word_counts = {}
	for key in lyric_dict:
		# at some point test if word vector is the correct length
		if not lyric_dict[key][0] in genre_word_counts:
			genre_word_counts[lyric_dict[key][0]] = lyric_dict[key][1]
		else:
			for x in range(len(lyric_dict[key][1])):
				genre_word_counts[lyric_dict[key][0]][x] = genre_word_counts[lyric_dict[key][0]][x] + lyric_dict[key][1][x]				
	return genre_word_counts

def words_to_probs(words):
	probs = {}
	for key in words:
		probs[key] = normalise(words[key])
	return probs

def normalise(input):	
	no_words = 0
	output = [None] * len(input)
	for x in range(len(input)):
		no_words = no_words + input[x]
	for x in range(len(input)):
		output[x] = input[x]/no_words
	return output
	
def which_genre(new_track, parameter_vectors):
	# new track is just list of word count
	probabilities = {}
	for key in parameter_vectors:
		probabilities[key] = get_probabilities(new_track, parameter_vectors[key])
	return probabilities
	
def get_probabilities(track, parameters):
	no_words = 0
	for x in range(len(track)):
		no_words = no_words + track[x]
	prob = math.factorial(no_words)
	if len(track) != len(parameters):
		#error
		return 0
	else:
		for x in range(len(track)):
			prob = prob * math.pow(parameters[x], track[x])
			prob = prob / math.factorial(track[x])
	return prob
		
my_dict = {'track1':['rock', [1, 2, 0, 2, 1]], 'track2':['pop', [2, 0, 0, 1, 3]], 'track3':['rock', [2, 2, 3, 1, 0]]}
word_counts = get_word_counts(my_dict)
print(word_counts)
parameters = words_to_probs(word_counts)
print(parameters)
scores = which_genre([2, 0, 0, 1, 4], parameters)
print(scores)

 