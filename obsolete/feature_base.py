import string
# NEEDS UPDATE TO SQL

class Feature:

	def __init__(self, infile, operation):
		self.infile = infile
		self.operation = operation
		
	def operate(self):
		# needs more

		print('Parsing dataset') # debug
		
		# Create parsed file
		oldmainfile = open('Data/' + self.infile + '.txt', 'r')
		newmainfile = open('Data/' + self.infile + "_" + self.operation + '.txt','w')
		
#		Operate on each line to get new score
		for line in oldmainfile:
			# parse our data
			newline = line.split(',',2)
			preline = newline[:2]
			preline = ','.join(str(e) for e in preline)
			newline = newline[2:]
			newline = newline[0].split(',')
			
			# operate on lyrics
			newline = self.operate_feature(newline)
			
			# reappend
			outline = preline + ',' + str(newline) + '\n'
			
			# write to new file
			newmainfile.write(outline)
			
		oldmainfile.close()
		newmainfile.close()
					
	def operate_feature(self, line): # TEMP
		return ""