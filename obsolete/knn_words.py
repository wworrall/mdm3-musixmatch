import numpy as np
import features
from classifier import run_classifier
from utils import sparse_word_count_matrix
from utils import get_genre_list
from sklearn.neighbors import KNeighborsClassifier

#doesnt work so well
def main():
	#get data
	Xtest = sparse_word_count_matrix(is_test=1)
	Xtrain = sparse_word_count_matrix()
	print("Got Features")
	ytest = get_genre_list(is_test=1)
	ytrain = get_genre_list()
	print("Got Labels")
	
	clf = KNeighborsClassifier()
	run_classifier(clf, "knn words", Xtest, Xtrain, ytest, ytrain)
	
	
if __name__ == "__main__":
	main()
