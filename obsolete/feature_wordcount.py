import feature_base
import string

class FeatureWordCount(feature_base.Feature):
	
	def operate_feature(self, line):
		wordcount = 0
		for i, val in enumerate(line):
			val = val.split(':',1)[-1]
			wordcount = wordcount + int(val)
		return wordcount
	
def main():
	featuretrial = FeatureWordCount("sampledata","wordcount")
	featuretrial.operate()


if __name__ == "__main__":
	main()