import numpy as np
import matplotlib.pyplot as plt
from utils import sum_genres
import features

X = {}
genre_counts = sum_genres()
for genre in genre_counts:
	X[genre] = [features.feature("word_count",genre), features.feature("unique_word_count",genre)] 

X2 = X.copy()
for genre1 in X:
	del X2[genre1]
	for genre2 in X2:
			to_plot1 = (np.array(X[genre1]))
			to_plot2 = (np.array(X[genre2]))
			fig = plt.figure()
			plt.scatter(to_plot1[0], to_plot1[1], c="red")
			plt.scatter(to_plot2[0], to_plot2[1], c="blue")
			im_name = "figs/" + str(genre1) + str(genre2)
			plt.savefig(im_name)
			plt.close(fig)
	