#REQUIRES NLTK
from nltk.corpus import wordnet
from nltk.corpus import sentiwordnet
from nltk.corpus import stopwords
import linecache
import numpy

def main():
	#load words line
	print("loading mxm words line...")
	test_words_line = linecache.getline('C:/Users/joeli/Desktop/Code/data/mxm_dataset_test.txt', 18)[1:]
	test_words_list = test_words_line.split(",")
	
	#load mxm mapping
	#create dictionary from mapping
	print("creating dictionary from mxm mapping...")
	dictList = {}
	with open('C:/Users/joeli/Desktop/Code/data/mxm_reverse_mapping.txt', 'r') as f:
		for line in f:
			(first,second)=line.rstrip().split("<SEP>")
			dictList[first]=second		
	#update words list using dictionary
	print("updating words list using dictionary...")
	updated_list=[]
	for w in test_words_list:
		updated_list.append(dictList.get(w.rstrip(), w.rstrip()))		
	
	#load stopwords_list
	print("updating words list using english stop words...")
	stops_english = set(stopwords.words('english'))
	updated_list_2 = [word for word in updated_list if word.lower() not in stops_english]			
	
	#save updated words_list
	#print("saving words_list...")
	#with open('words_list.txt','w') as thefile:
	#	for item in updated_list_2:
	#		try:
	#			thefile.write("%s\n" % item)	
	#		except:
	#			thefile.write("%.4f\n" % -1)
	#print("saved to \"words_list.txt\"")
	
	#get pos/neg/obj values
	print("getting sentiment values...")
	with open('sentiment_array.txt','w') as thefile:
		for w in updated_list_2:
			try:
				sent=sentiment(w)
				thefile.write("%.4f\t%.4f\t%.4f\n" % (sent[0],sent[1],sent[2]))
			except:
				print(updated_list_2.index(w))
	print("saved to \"sentiment_array.txt\"")
	
	#find proportion of pos/obj/neg in each track
	#for track in tracks
	#	for word in track
	#		pos_coutner=(word.pos*word_occurence)+pos_counter 
	#		obj_counter=(word.obj*word_occurence)+obj_counter
	#		neg_counter=(word.neg*word_occurence)+neg_counter
	#	track_pos=pos_counter/total_words
	#	track_obj=obj_counter/total_words
	#	track_neg=neg_counter/total_words
	
	
#get pos/obj/neg sentiment value for word w
def sentiment(w):
	all_pos=numpy.empty(shape=(0, 0))
	all_neg=numpy.empty(shape=(0, 0))
	all_obj=numpy.empty(shape=(0, 0))
	for x in list(sentiwordnet.senti_synsets(w)):
		all_pos=numpy.append(all_pos,x.pos_score())
		all_neg=numpy.append(all_neg,x.neg_score())
		all_obj=numpy.append(all_obj,x.obj_score())
	return (numpy.mean(all_pos),numpy.mean(all_obj),numpy.mean(all_neg))
	
	
if __name__ == "__main__":
	main()