import numpy as np
import matplotlib.pyplot as plt
from utils import sum_genres
import features
import math
import utils

# USE THIS
featurex = utils.feature_names
# TESTING
# featurex = ['word_count', 'unique_word_count']

featurex2 = featurex
for feat1 in featurex:
	featurex2.remove(feat1)
	for feat2 in featurex2:
		X = {}
		Xmax = {}
		genre_counts = sum_genres()
		for genre in genre_counts:
			X[genre] = [features.feature(feat1,genre),features.feature(feat2,genre)]
			# Normalise data here
#			Xmax[genre] = (max(X[genre]))
#			print(Xmax[genre])

		p1avg = {}
		p2avg = {}
		X2 = X.copy()
		for genre1 in X:
#			print(max(max(X[genre1])))
			del X2[genre1]
			
			to_plot1 = X[genre1]
#			print(len(to_plot1[0]))
			to_plot1[0] = [x for x in to_plot1[0] if x is not None]
			to_plot1[1] = [x for x in to_plot1[1] if x is not None]
#			print(to_plot1)
#			to_plot1 = to_plot1[np.nonzero(to_plot1)]
#			print(len(to_plot1[0]))
			p1avg[0] = sum(to_plot1[0]) / float(len(to_plot1[0]))
			p1avg[1] = sum(to_plot1[1]) / float(len(to_plot1[1]))
			p1avg[0] = p1avg[0] / max(to_plot1[0])
			p1avg[1] = p1avg[1] / max(to_plot1[1])
			for genre2 in X2:
					to_plot2 = X[genre2]
#					print(len(to_plot2[0]))
					to_plot2[0] = [x for x in to_plot2[0] if x is not None]
					to_plot2[1] = [x for x in to_plot2[1] if x is not None]
#					to_plot2 = to_plot2[np.nonzero(to_plot2)]
#					print(len(to_plot2[0]))
					p2avg[0] = sum(to_plot2[0]) / float(len(to_plot2[0]))
					p2avg[1] = sum(to_plot2[1]) / float(len(to_plot2[1])) 
					p2avg[0] = p2avg[0] / max(to_plot2[0])
					p2avg[1] = p2avg[1] / max(to_plot2[1])

		#			fig = plt.figure()
		#			plt.scatter(p1avg[0], p1avg[1], c="red")
		#			plt.scatter(p2avg[0], p2avg[1], c="blue")
					im_name = str(feat1) + str(feat2) + str(genre1) + str(genre2)
					Xdiff = abs(p1avg[0] - p2avg[0])
					Ydiff = abs(p1avg[1] - p2avg[1])
		#			plt.savefig(im_name)
		#			plt.close(fig)
					dist = math.sqrt(Xdiff*Xdiff + Ydiff*Ydiff)
					print(str(dist) + "   : " + im_name)
