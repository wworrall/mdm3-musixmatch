import utils
import numpy as np
import matplotlib.pyplot as plt

def main():
	a = np.random.rand(15,15)
	print(a)
	
	plt.imshow(a)
	plt.show()

if __name__ == "__main__":
	main()