import utils
from sklearn.naive_bayes import GaussianNB
from classifier import run_classifier
import numpy as np

def main():
    data = utils.get_data(train_len=1000, test_len=100)

    Xtrain = data["Xtrain"]
    Xtest = data["Xtest"]
    ytrain = data["ytrain"] 
    ytest = data["ytest"]

    clf = GaussianNB()
    run_classifier(clf, "Naive_bayes features", Xtest, Xtrain, ytest, ytrain)



if __name__ == "__main__":
	main()
