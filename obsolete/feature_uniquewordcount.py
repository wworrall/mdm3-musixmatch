import feature_base
import string

class FeatureWordCount(feature_base.Feature):
	
	def operate_feature(self, line):
		uniquecount = len(line)
		return uniquecount
	
def main():
	featuretrial = FeatureWordCount("sampledata","uniquewordcount")
	featuretrial.operate()


if __name__ == "__main__":
	main()