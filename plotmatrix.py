import numpy as np
import matplotlib.pyplot as plt
from utils import sum_genres
from utils import feature_names
import features

featurenames = feature_names
featurenames.remove('language')
X = {}
genre_counts = sum_genres()
for genre in genre_counts:
	X[genre] = []
	for i in featurenames:
		X[genre].append(features.feature(i,genre))

for feat1 in range(0,len(featurenames)):
	for feat2 in range(feat1,len(featurenames)):
		fig=plt.figure()
		if feat1 == feat2:
			to_plot1 = []
			for genre in X:
				to_plot1 = to_plot1 + (X[genre][feat1])
			to_plot1 = [x for x in to_plot1 if x is not None]
			plt.hist(to_plot1,100)
			plt.ylabel('Frequency')
		else:
			for genre in X:
				to_plot1 = (X[genre][feat1])
				to_plot2 = (X[genre][feat2])
				plt.scatter(to_plot1, to_plot2, c="red")	
			plt.ylabel(featurenames[feat2].replace('_',' ').title())
		im_name = "pmatrix/" + str(featurenames[feat1]) + "_against_" + str(featurenames[feat2])
		plt.xlabel(featurenames[feat1].replace('_',' ').title())
		plt.title(im_name.split('/',1)[1].replace('_',' ').title())
		plt.savefig(im_name)
		print(im_name)
		plt.close(fig)
	