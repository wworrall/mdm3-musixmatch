import features
import utils
import numpy as np
from sklearn import tree
from random import randint
from sklearn.externals.six import StringIO
import sklearn.metrics as skl_metrics
from sklearn.preprocessing import Imputer
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import recall_score
import matplotlib.pyplot as plt

def main():

	Xtrain, Xtest, ytrain, ytest = utils.get_data(train_size=200, test_size=25)
	classifiers = {}
	#classifiers["Tree"] = tree.DecisionTreeClassifier(max_depth=1)
	classifiers["Random Forest"] = RandomForestClassifier(max_depth=5)
		
	for classifier in sorted(classifiers):
		run_classifier(classifiers[classifier], classifier, Xtest, Xtrain,ytest, ytrain)	
	
	return classifiers

def run_classifier(clf, classifier_name, Xtest, Xtrain, ytest, ytrain):	
	# build classifier and export decision tree
	clf = clf.fit(Xtrain, ytrain)
	
	if classifier_name == "Tree":
		with open("tree1.dot", 'w') as f:
			f = tree.export_graphviz(clf, out_file=f)

# 	test decision tree on test data	
	predictions = clf.predict(Xtest)
	conf_matrix = skl_metrics.confusion_matrix(ytest,
                predictions, labels=utils.genres)
	acc = skl_metrics.accuracy_score(ytest, predictions)
	wap = skl_metrics.precision_score(ytest, predictions,
                average='weighted',pos_label=None)
	prec = skl_metrics.precision_score(ytest, predictions,
                average=None)
	recall = skl_metrics.recall_score(ytest, predictions,
                average=None)
	print(utils.format_conf_matrix(conf_matrix))
	print("Performance of " + classifier_name + " classifier:")
	print("Weighted Average Precision: " + str(wap))
	print("Accuracy: " + str(acc) + "\n")
	print("Precisions: " + " ".join([ str(x) for x in prec ]))
	print("Recall: " + " ".join([ str(x) for x in recall ]))
	
	fig = plt.figure()
	plt.imshow(conf_matrix, interpolation='none', cmap="Blues")
	im_name = classifier_name + "_conf_matrix"
	plt.savefig(im_name)
	plt.close(fig)

if __name__ == "__main__":
	main()
